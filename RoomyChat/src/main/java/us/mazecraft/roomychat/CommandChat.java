package us.mazecraft.roomychat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;

import static us.mazecraft.roomychat.Main.*;

/**
 * Created by AlteranKing on 4/9/2016.
 */

public class CommandChat implements CommandExecutor {

    private Main plugin;

    public CommandChat(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (cmd.getName().equalsIgnoreCase("chat") || cmd.getName().equalsIgnoreCase("c")) {
                if (args.length == 0) {
                    ArrayList<String> main = roomControl.getMain(player);
                    if (main != null) {
                        player.sendMessage("§6You're chatting in room: §b" + main.get(0));
                        ArrayList<String> rooms = roomControl.getPlayerRooms(player);
                        player.sendMessage("§6Rooms you're listening to:");
                        for (int i = 0; i < rooms.size(); i++) {
                            ArrayList<String> room = roomControl.getRoomInfo(rooms.get(i));
                            if (Integer.parseInt(room.get(3)) > 0) {
                                if (room.get(5).equalsIgnoreCase("true")) {
                                    player.sendMessage(format(String.format("%s%s%s%s%s%s%s%s", "§f[§c", room.get(4), "§f/§c", room.get(3), "§f]§6 - §3", room.get(0), ":§e ", room.get(2))));
                                } else {
                                    player.sendMessage(format(String.format("%s%s%s%s%s%s%s%s", "§f[§a", room.get(4), "§f/§6", room.get(3), "§f]§6 - §3", room.get(0), ":§e ", room.get(2))));
                                }
                            } else {
                                player.sendMessage(format(String.format("%s%s%s%s%s%s", "§f[§a", room.get(4), "§f/§dU§f]§6 - §3", room.get(0), ":§e ", room.get(2))));
                            }
                        }
                    }
                    else{
                        if(roomControl.getPlayerRooms(player).isEmpty()){
                            player.sendMessage(format("&cYou're not chatting in any rooms yet. Add a room with /chat join {chatroom}."));
                            player.sendMessage(format("&cView available rooms with /chat rooms."));
                        }
                        else{
                            ArrayList<String> rooms = roomControl.getPlayerRooms(player);
                            player.sendMessage("§6Rooms you're listening to:");
                            for (int i = 0; i < rooms.size(); i++) {
                                ArrayList<String> room = roomControl.getRoomInfo(rooms.get(i));
                                if (Integer.parseInt(room.get(3)) > 0) {
                                    if (room.get(5).equalsIgnoreCase("true")){
                                        player.sendMessage(format(String.format("%s%s%s%s%s%s%s%s", "§f[§c", room.get(4), "§f/§c", room.get(3), "§f]§6 - §3", room.get(0), ":§e ", room.get(2))));
                                    } else {
                                        player.sendMessage(format(String.format("%s%s%s%s%s%s%s%s", "§f[§a", room.get(4), "§f/§6", room.get(3), "§f]§6 - §3", room.get(0), ":§e ", room.get(2))));
                                    }
                                } else {
                                    player.sendMessage(format(String.format("%s%s%s%s%s%s", "§f[§a", room.get(4), "§f/§dU§f]§6 - §3", room.get(0), ":§e ", room.get(2))));
                                }
                            }
                            player.sendMessage("§cYou're not chatting in a room for some reason. Set one of the rooms above as your main chatroom with /chat set");
                        }
                    }
                    return true;
                }
                else if (args[0].equalsIgnoreCase("create")){
                    if (player.hasPermission("chat.create")){
                        if(args.length > 1){
                            String room = args[1];
                            if (roomControl.exists(room)){
                                player.sendMessage("§cRoom already exists!");
                                return true;
                            }
                            else{
                                roomControl.addRoom(room);
                                player.sendMessage(String.format("%s%s%s", "§6Room §b", room, "§6 successfully created!"));
                                return true;
                            }
                        }
                        else{
                            player.sendMessage("§cMake sure to specify a room name!");
                            return true;
                        }
                    }
                    else {
                        player.sendMessage("§cSorry but you don't have permission to do that!");
                        return true;
                    }
                }
                else if (args[0].equalsIgnoreCase("delete") || args[0].equalsIgnoreCase("remove")){
                    if (player.hasPermission("chat.delete")) {
                        if (args.length > 1) {
                            String room = args[1];
                            if (roomControl.exists(room)) {
                                roomControl.removeRoom(room);

                                player.sendMessage(String.format("%s%s%s", "§6Room §b", room, "§6 successfully removed."));
                                return true;
                            } else {
                                player.sendMessage(String.format("%s%s%s", "§cRoom §b", room, "§c doesn't exist!"));
                                return true;
                            }
                        } else {
                            player.sendMessage("§cMake sure to specify a room name!");
                            return true;
                        }
                    }
                    else {
                        player.sendMessage("§cSorry but you don't have permission to do that!");
                        return true;
                    }
                }
                else if (args[0].equalsIgnoreCase("help")){
                    player.sendMessage("§6Usage: /chat §6- see what rooms you're in");
                    player.sendMessage("§cExample: §3/chat §bjoin §6[§bchatroom§6] §6- join a chatroom");
                    player.sendMessage("§cExample: §3/chat §bleave §6[§bchatroom§6] §6- leave a chatroom");
                    player.sendMessage("§cExample: §3/chat §bset §6[§bchatroom§6] §6- set chatroom to talk in");
                    player.sendMessage("§cExample: §3/chat §brooms §6- view all available rooms");
                    if(player.hasPermission("chat.create"))
                        player.sendMessage("§cExample: §3/chat §bcreate §6[§bchatroom§6] §6- create a new chatroom to talk in");
                    if(player.hasPermission("chat.delete"))
                        player.sendMessage("§cExample: §3/chat §bdelete §6[§bchatroom§6] §6- delete a chatroom");
                    if(player.hasPermission("chat.adddef")){
                        player.sendMessage("§cExample: §3/chat §badddef §6[§bchatroom§6] §6- adds a chatroom to the one's people automatically join");
                        player.sendMessage("§cExample: §3/chat §bremdef §6[§bchatroom§6] §6- removes a chatroom from the one's people automatically join");
                    }
                    if(player.hasPermission("chat.setprefix"))
                        player.sendMessage("§cExample: §3/chat §bsetprefix §6[§bchatroom§6] §6[§bprefix§6]- change the prefix of a chatroom");
                    if(player.hasPermission("chat.setlimit"))
                        player.sendMessage("§cExample: §3/chat §bsetlimit §6[§bchatroom§6] §6[§blimit§6]- change the limit of chatters in a chatroom");
                    if(player.hasPermission("chat.setd"))
                        player.sendMessage("§cExample: §3/chat §bsetd §6[§bchatroom§6] §6[§bdescription§6]- change the description of a chatroom");
                    if(player.hasPermission("chat.config")){
                        player.sendMessage("§cExample: §3/chat §bconfig - view and change config options of RoomyChat");
                    }
                    return true;
                }
                else if (args[0].equalsIgnoreCase("join")){
                    if (args.length < 2){
                        player.sendMessage("§cPlease enter a room to join.");
                    }
                    else{
                        String roomToJoin = args[1];
                        if(roomControl.exists(roomToJoin)){
                            if (player.hasPermission("chat.join.allow." + roomToJoin) && !player.hasPermission("chat.join.deny." + roomToJoin)) {
                                if (!roomControl.getPlayerRooms(player).contains(roomToJoin)){
                                    if (roomControl.getRoomInfo(roomToJoin).get(5).equalsIgnoreCase("true")){
                                        player.sendMessage("§cSorry, but that room is full!");
                                    }
                                    else {
                                        roomControl.addPlayerToRoom(player, roomToJoin);
                                        roomControl.setMain(player, roomToJoin);
                                        for(Player roommate : roomControl.getRoomPlayers(roomToJoin)){
                                            roommate.sendMessage(String.format("%s%s%s%s%s", "§b", player.getDisplayName(), "§6 has joined §b", roomToJoin, "§6!"));
                                        }
                                    }

                                }
                                else {
                                    player.sendMessage("§cYou've already joined that room!");
                                }

                            }
                            else {
                                player.sendMessage("§cSorry, that room doesn't exist");
                            }
                        }
                        else{
                            player.sendMessage("§cSorry, that room doesn't exist");
                        }
                        return true;
                    }
                }
                else if (args[0].equalsIgnoreCase("leave")){
                    if (args.length == 2){
                        String roomToLeave = args[1];
                        if(roomControl.exists(roomToLeave)){
                            if (!player.hasPermission("chat.leave.deny." + roomToLeave)) {
                                if (roomControl.getPlayerRooms(player).contains(roomToLeave)){
                                    roomControl.removePlayerFromRoom(player, roomToLeave);
                                    player.sendMessage("§6You left §b" + roomToLeave);
                                }
                                else {
                                    player.sendMessage("§cSorry, you're not apart of that room!");
                                }
                            }
                            else {
                                player.sendMessage("§cSorry, you don't have permission to leave this chat.");
                            }
                        }
                        else{
                            player.sendMessage("§cSorry, that room doesn't exist");
                        }
                    }
                    else
                        player.sendMessage("§cPlease enter a room to leave.");
                    return true;
                }
                else if (args[0].equalsIgnoreCase("room") || args[0].equalsIgnoreCase("rooms")){
                    ArrayList<room> rooms = roomControl.getRooms();
                    player.sendMessage("§6Rooms you can join: ");
                    for(int i = 0; i < rooms.size(); i++){
                        if (player.hasPermission("chat.join.allow." + rooms.get(i).getName()) && !player.hasPermission("chat.join.deny." + rooms.get(i).getName()))  {
                            if (rooms.get(i).getLimit() > 0) {
                                if (rooms.get(i).isFull()) {
                                    player.sendMessage(format(String.format("%s%s%s%s%s%s", "§f[§c", rooms.get(i).getChatters(), "§f/§c", rooms.get(i).getLimit(), "§f]§6 - §c", rooms.get(i).getName())));
                                } else {
                                    player.sendMessage(format(String.format("%s%s%s%s%s%s%s%s", "§f[§a", rooms.get(i).getChatters(), "§f/§6", rooms.get(i).getLimit(), "§f]§6 - §3", rooms.get(i).getName(), ":§e ", rooms.get(i).getDescription())));
                                }
                            } else {
                                player.sendMessage(format(String.format("%s%s%s%s%s%s", "§f[§a", rooms.get(i).getChatters(), "§f/§dU§f]§6 - §3", rooms.get(i).getName(), ":§e ", rooms.get(i).getDescription())));
                            }
                        }

                    }
                    return true;
                }
                else if (args[0].equalsIgnoreCase("set")){
                    if (args.length > 1) {
                        String room = args[1];
                        if(roomControl.exists(room)){
                            if (player.hasPermission("chat.join.allow." + room) && !player.hasPermission("chat.join.deny." + room)) {
                                roomControl.addPlayerToRoom(player, room);
                                roomControl.setMain(player, room);
                                player.sendMessage("§6You're now chatting in §b" + room);
                            }
                            else {
                                player.sendMessage("§cSorry, you don't have permission to join that room.t");
                            }
                        }
                        else {
                            player.sendMessage("§cSorry, that room doesn't exist.");
                        }
                    }
                    else {
                        player.sendMessage("§cPlease enter a room.");
                    }
                    return true;
                }
                else if (args[0].equalsIgnoreCase("adddef")){
                    if(args.length > 1){
                        String room = args[1];
                        if(roomControl.exists(room)){
                            if (player.hasPermission("chat.adddef")) {
                                roomControl.addDefaultRoom(room);
                                player.sendMessage(String.format("%s%s%s", "§b", room, "§6 is now a default room."));
                            }
                            else {
                                player.sendMessage("§cSorry, you don't have permission to set defaults");
                            }
                        }
                        else {
                            player.sendMessage("§cSorry, that room doesn't exist. Try and create it first.");
                        }
                        return true;
                    }
                    else {
                        player.sendMessage("§cPlease enter a room!");
                    }
                }
                else if (args[0].equalsIgnoreCase("remdef")){
                    if(args.length > 1){
                        String room = args[1];
                        if(roomControl.exists(room)){
                            if (player.hasPermission("chat.adddef")) {

                                roomControl.removeDefaultRoom(room);
                                player.sendMessage(String.format("%s%s%s", "§b", room, "§6 is no longer a default room."));
                            }
                            else {
                                player.sendMessage("§cSorry, you don't have permission to set defaults");
                            }
                        }
                        else {
                            player.sendMessage("§cSorry, that room already doesn't exist.");
                        }
                        return true;
                    }
                    else {
                        player.sendMessage("§cSPlease enter a room!");
                    }
                }
                else if (args[0].equalsIgnoreCase("setprefix")){
                    if(args.length > 1){
                        String room = args[1];
                        if(roomControl.exists(room)){
                            if (player.hasPermission("chat.setprefix." + room)) {
                            String prefix = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
                            roomControl.setRoomPrefix(room, prefix);
                            player.sendMessage(format(String.format("%s%s%s%s", "§b", room, "§6 now has the prefix: ", prefix)));
                            }
                            else {
                            player.sendMessage("§cSorry, you don't have permission to set prefixes");
                            }
                        }
                        else {
                            player.sendMessage(String.format("%s%s%s", "§cSorry, ", room, " doesn't exist. Try and create it first."));
                        }
                        return true;
                    }
                    else {
                        player.sendMessage("§cSorry, but you didn't enter a room or a prefix");
                    }
                }
                else if (args[0].equalsIgnoreCase("setlimit")){
                    if(args.length > 1){
                        String room = args[1];
                        if(roomControl.exists(room)){
                            if(room.equalsIgnoreCase("global") || roomControl.getWorldRooms().contains(room)){
                                player.sendMessage("§cSorry, but you change the limit of that room!");
                            }
                            else{
                                if (player.hasPermission("chat.setlimit." + room)) {
                                    if(args.length > 2){
                                        if(isInteger(args[2])){
                                            Integer actualLimit = roomControl.setRoomLimit(room, Integer.parseInt(args[2]));
                                            player.sendMessage(String.format("%s%s%s%s", "§b", room, "§6 now has the limit: §b", actualLimit.toString()));
                                        }
                                        else{
                                            player.sendMessage("§cSorry, but you didn't enter a number.");
                                        }
                                    }
                                    else
                                    {
                                        player.sendMessage("§cSorry, but you didn't enter a number.");
                                    }
                                }
                                else {
                                    player.sendMessage("§cSorry, you don't have permission to set limits");
                                }
                            }
                        }
                        else {
                            player.sendMessage(String.format("%s%s%s", "§cSorry, ", room, " doesn't exist. Try and create it first."));
                        }
                        return true;
                    }
                    else {
                        player.sendMessage("§cSorry, but you didn't enter a room or a limit.");
                    }
                }
                else if (args[0].equalsIgnoreCase("setd")){
                        if(args.length > 1){
                            String room = args[1];
                            if (player.hasPermission("chat.setd." + room)){
                                if(roomControl.exists(room)){
                                    String desc = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
                                    roomControl.setRoomDescription(room, desc);
                                    player.sendMessage(format(String.format("%s%s%s%s", "§b", room, "§6 now has the definition: ", desc)));
                                }
                                else {
                                    player.sendMessage(String.format("%s%s%s", "§cSorry, ", room, " doesn't exist. Try and create it first."));
                                }
                                return true;
                            }
                            else {
                                player.sendMessage("§cSorry, you don't have permission to set descriptions");
                            }
                        }
                        else {
                            player.sendMessage("§cSorry, but you didn't enter a room or a description");
                        }

                }
                else if (roomControl.exists(args[0])){
                    if (args.length == 1) {
                        String room = args[0];
                        if (player.hasPermission("chat.join.allow." + room) && !player.hasPermission("chat.join.deny." + room)) {
                            roomControl.addPlayerToRoom(player, room);
                            roomControl.setMain(player, room);
                            player.sendMessage("§6You're now chatting in §b" + room);
                        }
                        else {
                            player.sendMessage("§cSorry, that room doesn't exist.");
                        }
                    }
                    else {
                        player.sendMessage("§cPlease enter just one room.");
                    }
                    return true;
                }
                else if (args[0].equalsIgnoreCase("config")){
                    if(player.hasPermission("chat.config")){
                        if (args.length > 1){
                            String option = args[1];
                            if (option.equalsIgnoreCase("global")){
                                if (args.length > 2){
                                    if (args[2].equalsIgnoreCase("true")){
                                        global = true;
                                        player.sendMessage("§3Global§6 is now set to §btrue.");
                                    }
                                    else if (args[2].equalsIgnoreCase("false")){
                                        global = false;
                                        player.sendMessage("§6Global§3 is now set to §bfalse.");
                                    }
                                    else
                                        player.sendMessage("§cSorry, the only acceptable values are §btrue§c and §bfalse.");
                                }
                                else{
                                    player.sendMessage("§3Global §6(having a global chat room) is set to: §b" + global.toString());
                                }
                            }
                            else if (option.equals("global_default")){
                                if (args.length > 2){
                                    if (args[2].equalsIgnoreCase("true")){
                                        global_default = true;
                                        player.sendMessage("§3Global_default§6 is now set to §btrue");
                                    }
                                    else if (args[2].equalsIgnoreCase("false")){
                                        global_default = false;
                                        player.sendMessage("§3Global_default §6is now set to §bfalse");
                                    }
                                    else
                                        player.sendMessage("§cSorry, the only acceptable values are §btrue §cand §bfalse.");
                                }
                                else{
                                    player.sendMessage("§3Global_default §6(everyone auto-joins global chat room) is set to: §b" + global_default.toString());
                                }
                            }
                            else if (option.equalsIgnoreCase("world_rooms")){
                                if (args.length > 2){
                                    if (args[2].equalsIgnoreCase("true")){
                                        world_rooms = true;
                                        player.sendMessage("§3World_rooms §6is now set to §btrue");
                                    }
                                    else if (args[2].equalsIgnoreCase("false")){
                                        world_rooms = false;
                                        player.sendMessage("§3World_rooms §6is now set to §bfalse");
                                    }
                                    else
                                        player.sendMessage("§cSorry, the only acceptable values are §btrue §cand §bfalse.");
                                }
                                else{
                                    player.sendMessage("§3World_rooms §6(having a room for each world) is set to: §b" + world_rooms.toString());
                                }
                            }
                            else if (option.equalsIgnoreCase("world_switching")){
                                if (args.length > 2){
                                    if (args[2].equalsIgnoreCase("true")){
                                        world_switching = true;
                                        player.sendMessage("§3World_switching §6is now set to §btrue.");
                                    }
                                    else if (args[2].equalsIgnoreCase("false")){
                                        world_switching = false;
                                        player.sendMessage("§3World_switching §6is now set to §bfalse");
                                    }
                                    else
                                        player.sendMessage("§cSorry, the only acceptable values are §btrue §cand §bfalse.");
                                }
                                else{
                                    player.sendMessage("§3World_switching §6(players switch rooms when switching worlds) is set to: §b" + world_switching.toString());
                                }
                            }
                            else if (option.equalsIgnoreCase("on_join")){
                                if (args.length > 2){
                                    if (args[2].equalsIgnoreCase("global")){
                                        on_join = "global";
                                        player.sendMessage("§3On_join §6is now set to §bglobal");
                                    }
                                    else if (args[2].equalsIgnoreCase("world")){
                                        on_join = "world";
                                        player.sendMessage("§3On_join §6is now set to §bworld");
                                    }
                                    else if (args[2].equalsIgnoreCase("last_room")){
                                        on_join = "last_room";
                                        player.sendMessage("§3On_Join §6is now set to §blast_room");
                                    }

                                    else
                                        player.sendMessage("§cSorry, the only acceptable values are §bglobal§c, §bworld§c, and §blast_room.");
                                }
                                else{
                                    player.sendMessage("§3On join §6(the room players should join when logging in) is set to: §b" + on_join);
                                }
                            }
                            else if (option.equalsIgnoreCase("max_limit")){
                                if (args.length > 2){
                                    if (isInteger(args[2])){
                                        max_limit = Integer.parseInt(args[2]);
                                        player.sendMessage("§3Max_limit §6is now set to §b" + max_limit.toString());
                                    }
                                    else
                                        player.sendMessage("§cSorry, only numbers are acceptable values.");
                                }
                                else{
                                    player.sendMessage("§3Max_limit §6(the maximum number of players in a room) is set to: §b" + max_limit.toString());
                                }
                            }
                            else if (option.equalsIgnoreCase("default_limit")){
                                if (args.length > 2){
                                    if (isInteger(args[2])){
                                        default_limit = Integer.parseInt(args[2]);
                                        player.sendMessage("§3Default_limit §6is now set to §b" + default_limit.toString());
                                    }
                                    else
                                        player.sendMessage("§cSorry, only numbers are acceptable values.");
                                }
                                else{
                                    player.sendMessage("§3Default_limit §6(the maximum limit when creating a new room) is set to: §b" + default_limit.toString());
                                }
                            }
                            else if (option.equalsIgnoreCase("default_prefix")){
                                if (args.length > 2){
                                    default_prefix = String.join(" ", Arrays.copyOfRange(args, 2, args.length));
                                    player.sendMessage("§3Default_prefix §6is now set to: " + default_prefix);
                                }
                                else{
                                    player.sendMessage("§3Default_prefix §6is set to: " + default_prefix);
                                }
                            }
                            else if (option.equalsIgnoreCase("default_description")){
                                if (args.length > 2){
                                    default_description = String.join(" ", Arrays.copyOfRange(args, 1, args.length));
                                    player.sendMessage("§3Default_description §6is now set to: " + default_description);
                                }
                                else{
                                    player.sendMessage("§3Default_description §6is set to: " + default_description);
                                }
                            }
                            else if (option.equalsIgnoreCase("version")){
                                player.sendMessage("§6RoomyChat plugin version is: §b" + this.plugin.pluginVersion);
                            }
                            else if (option.equalsIgnoreCase("logging_level")){
                                if (args.length > 2){
                                    logging_level = args[2];
                                    player.sendMessage("§3Logging_level §6is now set to: §b" + logging_level);
                                }
                                else{
                                    player.sendMessage("§3Logging_level §6is set to: §b" + logging_level);
                                }
                            }
                            else if (option.equalsIgnoreCase("update_check")){
                                if (args.length > 2){
                                    if (args[2].equalsIgnoreCase("true")){
                                        update_check = true;
                                        player.sendMessage("§3Update_check §6is now set to §btrue.");
                                    }
                                    else if (args[2].equalsIgnoreCase("false")){
                                        update_check = false;
                                        player.sendMessage("§3Update_check §6is now set to §bfalse.");
                                    }
                                    else
                                        player.sendMessage("§cSorry, the only acceptable values are §btrue §cand §bfalse.");
                                }
                                else{
                                    if (!update_check){
                                        player.sendMessage("§cUpdate checker is turned §3off§c. Please turn it on to check for new updates.");
                                    }
                                    else{
                                        checkForUpdate();
                                        if(new_version){
                                            player.sendMessage("§dNew version of RoomyChat available!");
                                        }
                                        else{
                                            player.sendMessage("§6RoomyChat is up-to-date!");
                                        }
                                    }
                                }
                            }
                            else{
                                player.sendMessage("§cWhich option would you like to see/change? Available options are: global, global_default, world_rooms, world_switching, on_join, max_limit, default_limit, default_prefix, default description, version, update_check, logging_level");
                            }
                            return true;
                        }
                        else {
                            player.sendMessage("§cWhich option would you like to see/change? Available options are: global, global_default, world_rooms, world_switching, on_join, max_limit, default_limit, default_prefix, default description, version, update_check, logging_level");
                        }
                    }
                    else{
                        player.sendMessage("§cSorry, you don't have permission to change configurations.");
                    }
                }
                else if (args[0].equalsIgnoreCase("reload")){
                    if (player.hasPermission("chat.config")) {
                        player.sendMessage("§6Reloading RoomyChat.");
                        this.plugin.reload();
                        player.sendMessage("§6Successfully reloaded RoomyChat.");
                    }
                    else {
                    player.sendMessage("§cSorry, you don't have permission to reload RoomyChat");
                    }
                }
                else{
                    return false;
                }
            }

        }
        return true;
    }

    private boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(Exception e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }

}