package us.mazecraft.roomychat;

import org.bukkit.entity.Player;

import java.util.ArrayList;

import static us.mazecraft.roomychat.Main.*;

/**
 * Created by Patman on 7/2/2016.
 */
public class room{

    private ArrayList<Player> players = new ArrayList<>();
    private String name, description, prefix;
    private Integer limit, chatters;
    private Boolean pNeedsParsing = false;
    private Boolean dNeedsParsing = false;

    public room(String name) {
        this.name = name;
        this.players = new ArrayList<>();
        this.limit = setLimit(default_limit);
        this.chatters = 0;
        setPrefix(default_prefix);
        setDescription(default_description);
    }

    public room(String name, Integer limit, String prefix, String description) {
        this.name = name;
        this.players = new ArrayList<>();
        this.limit = setLimit(limit);
        this.chatters = 0;
        this.prefix = parsePrefix(prefix);
        this.description = parseDescription(description);
    }

    public void addPlayer(Player player){
        this.players.add(player);
        addChatter();
    }

    public void removePlayer(Player player){
        this.players.remove(player);
        removeChatter();
    }

    public ArrayList<Player> getPlayers(){
        return this.players;
    }

    public void setPrefix(String prefix){
        if (prefix.toLowerCase().contains("$limit") || prefix.toLowerCase().contains("$num")){
            this.pNeedsParsing = true;
            this.prefix = prefix;
        }
        else{
            this.pNeedsParsing = false;
            this.prefix = parsePrefix(prefix);
        }
    }

    public String getPrefix() {
        if(!pNeedsParsing)
            return this.prefix;
        else
            return parsePrefix(this.prefix);
    }

    public String getName() { return this.name;}

    public void setDescription(String description) {
        if (description.toLowerCase().contains("$limit") || description.toLowerCase().contains("$num")){
            this.dNeedsParsing = true;
            this.description = description;
        }
        else{
            this.dNeedsParsing = false;
            this.description = parsePrefix(description);
        }
    }

    public String getDescription() {
        if(!dNeedsParsing){
            return this.description;
        }
        else
            return parseDescription(this.description);
    }

    public Integer setLimit(Integer limit) {
        if(limit > max_limit && max_limit > 0)
            this.limit = max_limit;
        else if (limit < 0 && max_limit > 0)
            this.limit = max_limit;
        else
            this.limit = limit;
        return this.limit;
    }

    public Integer getLimit() { return this.limit;}

    private void addChatter() {
        this.chatters++;
    }

    private void removeChatter(){
        this.chatters--;
    }

    public Integer getChatters(){
        return this.chatters;
    }

    public boolean isFull(){
        if (this.limit > 0){
            return this.chatters >= this.limit;
        }
        else{
            return false;
        }
    }

    private String parsePrefix (String string){
        string = string.replace("$room", this.name);
        string = string.replace("$limit", this.limit.toString());
        string = string.replace("$num", this.chatters.toString());
        return string;
    }
    private String parseDescription (String string){
        string = string.replace("$room", this.name);
        string = string.replace("$limit", this.limit.toString());
        string = string.replace("$num", this.chatters.toString());
        return string;
    }
}
