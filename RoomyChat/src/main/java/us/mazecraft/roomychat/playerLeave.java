package us.mazecraft.roomychat;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Patman on 7/2/2016.
 */
public class playerLeave implements Listener{

    private Main plugin;

    public playerLeave(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        this.plugin.savePlayersRooms(event.getPlayer());
    }
}
