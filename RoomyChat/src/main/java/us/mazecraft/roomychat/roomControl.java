package us.mazecraft.roomychat;

import org.bukkit.entity.Player;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import java.util.*;

import static org.bukkit.Bukkit.getServer;

/**
 * Created by Patman on 7/3/2016.
 */

public class roomControl{

    private Main plugin;

    public roomControl(Main plugin) {
        this.plugin = plugin;
    }

    private static ArrayList<String> defaults = new ArrayList<>();
    private static ArrayList<room> rooms = new ArrayList<>();
    private static ArrayList<String> worldRooms = new ArrayList<>();
    private static Map<String, ArrayList<String>> playerRooms = new HashMap<>();
    private static Map<String, room> mains = new HashMap<>();

    private static void addPerms(String room){
        Permission permJoinAllow = new Permission("chat.join.allow." + room, PermissionDefault.TRUE);
        Permission permLeaveAllow = new Permission("chat.leave.allow." + room, PermissionDefault.TRUE);
        Permission permJoinDeny = new Permission("chat.join.deny." + room, PermissionDefault.FALSE);
        Permission permLeaveDeny = new Permission("chat.leave.deny." + room, PermissionDefault.FALSE);
        Permission permSetPrefix = new Permission("chat.setprefix." + room);
        Permission permSetLimit = new Permission("chat.setlimit." + room);
        Permission permSetD = new Permission("chat.setd." + room);

        permJoinAllow.addParent("chat.join.allow.*", true);
        permJoinDeny.addParent("chat.join.deny.*", true);
        permLeaveAllow.addParent("chat.leave.allow.*", true);
        permLeaveDeny.addParent("chat.leave.deny.*", true);
        permSetPrefix.addParent("chat.setprefix", true);
        permSetLimit.addParent("chat.setlimit", true);
        permSetD.addParent("chat.setd", true);

        getServer().getPluginManager().addPermission(permJoinAllow);
        getServer().getPluginManager().addPermission(permJoinDeny);
        getServer().getPluginManager().addPermission(permLeaveAllow);
        getServer().getPluginManager().addPermission(permLeaveDeny);
        getServer().getPluginManager().addPermission(permSetPrefix);
        getServer().getPluginManager().addPermission(permSetLimit);
        getServer().getPluginManager().addPermission(permSetD);
    }

    private static void removePerms(String room){
        Permission permJoinAllow = new Permission("chat.join.allow." + room);
        Permission permLeaveAllow = new Permission("chat.leave.allow." + room);
        Permission permJoinDeny = new Permission("chat.join.deny." + room);
        Permission permLeaveDeny = new Permission("chat.leave.deny." + room);
        Permission permSetPrefix = new Permission("chat.setprefix." + room);
        Permission permSetLimit = new Permission("chat.setlimit." + room);
        Permission permSetD = new Permission("chat.setd." + room);

        getServer().getPluginManager().removePermission(permJoinAllow);
        getServer().getPluginManager().removePermission(permJoinDeny);
        getServer().getPluginManager().removePermission(permLeaveAllow);
        getServer().getPluginManager().removePermission(permLeaveDeny);
        getServer().getPluginManager().removePermission(permSetPrefix);
        getServer().getPluginManager().removePermission(permSetLimit);
        getServer().getPluginManager().removePermission(permSetD);
    }

    private static room getRoom(String check){
        for(room room : rooms){
            if (room.getName().equals(check))
                return room;
        }
        return null;
    }

    public static boolean exists(String check){
        for(room room : rooms){
            if (room.getName().equals(check))
                return true;
        }
        return false;
    }

    public static ArrayList<String> getRoomInfo(String check){
        ArrayList<String> roomInfo = new ArrayList<>();
        for(room room : rooms){
            if (room.getName().equals(check)){
                roomInfo.add(0, room.getName());
                roomInfo.add(1, room.getPrefix());
                roomInfo.add(2, room.getDescription());
                roomInfo.add(3, room.getLimit().toString());
                roomInfo.add(4, room.getChatters().toString());
                roomInfo.add(5, String.valueOf(room.isFull()));
            }

        }
        return roomInfo;
    }

    public static ArrayList<Player> getRoomPlayers(String name){
        ArrayList<Player> players = new ArrayList<>();
        if(exists(name)){
            players = getRoom(name).getPlayers();
        }
        return players;
    }

    public static boolean setRoomPrefix(String name, String prefix){
        if(exists(name)){
            getRoom(name).setPrefix(prefix);
            return true;
        }
        return false;
    }

    public static Integer setRoomLimit(String name, Integer limit){
        if(exists(name)){
            return getRoom(name).setLimit(limit);
        }
        return 0;
    }

    public static boolean setRoomDescription(String name, String desc){
        if(exists(name)){
            getRoom(name).setDescription(desc);
            return true;
        }
        return false;
    }

    public static boolean addRoom(String name){
        if(!exists(name)){
            room newRoom = new room(name);
            rooms.add(newRoom);
            addPerms(name);
            return true;
        }
        else
            return false;

    }

    public static boolean addRoom(String name, Integer limit, String prefix, String description){
        if(!exists(name)){
            room newRoom = new room(name, limit, prefix, description);
            rooms.add(newRoom);
            addPerms(name);
            return true;
        }
        else
            return false;
    }

    public static boolean removeRoom(String name){
        if (exists(name)){
            room room = getRoom(name);
            ArrayList<Player> players = room.getPlayers();
            if(!players.isEmpty()) {
                //Be wary of concurrent modification issues?
                for (int i = 0; i < players.size(); i++) {
                    if (mains.get(players.get(i).getName()) == room) {
                        setMain(players.get(i), null);
                    }
                    removePlayerFromRoom(players.get(i), name);
                }
                if (defaults.contains(room.getName())) {
                    removeDefaultRoom(room.getName());
                }

            }
            removePerms(room.getName());
            rooms.remove(room);
            return true;
        }
        return false;
    }

    public static ArrayList<room> getRooms(){
        return rooms;
    }

    public static boolean addDefaultRoom(String room){
        if(exists(room)){
            defaults.add(room);
            return true;
        }
        else
            return false;


    }

    public static boolean removeDefaultRoom(String room){
        if(exists(room)) {
            defaults.remove(room);
            return true;
        }
        else
            return false;
    }

    public static ArrayList<String> getDefaults() { return defaults; }

    public static boolean addWorldRoom(String room){
        if(exists(room)){
            worldRooms.add(room);
            return true;
        }
        else
            return true;
    }

    public static boolean removeWorldRoom(String room){
        if(exists(room)){
            worldRooms.remove(room);
            return true;
        }
        else
            return false;
    }

    public static ArrayList<String> getWorldRooms() { return worldRooms;}

    public static boolean addPlayerToRoom(Player player, String name){
        if(exists(name)){
            ArrayList<String> tempList = new ArrayList<>();
            if (playerRooms.get(player.getName()) != null){
                tempList = new ArrayList<>(playerRooms.get(player.getName()));
                if(tempList.contains(name)) {
                    return false;
                }
            }

            room room = getRoom(name);

            if (room.isFull()){
                return false;
            }
            tempList.add(room.getName());
            room.addPlayer(player);
            playerRooms.put(player.getName(), tempList);
            return true;
        }
        else
            return false;

    }

    public static boolean removePlayerFromRoom(Player player, String name){
        if(exists(name)){
            room room = getRoom(name);
            ArrayList<String> tempList = new ArrayList<>(roomControl.playerRooms.get(player.getName()));
            if(tempList.contains(room.getName())){
                room.removePlayer(player);
                tempList.remove(room.getName());
                roomControl.playerRooms.put(player.getName(), tempList);
                if (room == mains.get(player.getName())){
                    if (tempList.size() > 0){
                        setMain(player, getRoom(tempList.get(0)).getName());
                    }
                    else{
                        setMain(player, null);
                    }
                }
                return true;
            }
            else{
                return false;
            }
        }
        else
            return false;

    }

    public static ArrayList<String> getPlayerRooms(Player player){
        return roomControl.playerRooms.get(player.getName());
    }

    public static boolean setMain(Player player, String name){
        try{
            if(exists(name)){
                ArrayList<String> tempList = new ArrayList<>(roomControl.playerRooms.get(player.getName()));
                if(tempList.contains(name)){
                    roomControl.mains.put(player.getName(), getRoom(name));
                    return true;
                }
                else{
                    return false;
                }
            }
            else
                return false;
        }
        catch (NullPointerException e){
            roomControl.mains.put(player.getName(), null);
            return true;
        }

    }

    public static ArrayList<String> getMain(Player player){
        if (roomControl.mains.containsKey(player.getName())){
            return getRoomInfo(mains.get(player.getName()).getName());
        }
        else{
            return null;
        }
    }

    public static void removeAll(Player player){
        try {
            mains.remove(player.getName());
            ArrayList<String> tempList = new ArrayList<>(playerRooms.get(player.getName()));
            for(String room : tempList){
                getRoom(room).removePlayer(player);
            }
            playerRooms.remove(player.getName());
        }
        catch (NullPointerException e){
            // Do nothing?
        }
    }

    public static void removeAllRooms(){
        rooms.clear();
    }

    public static void removeAllDefaults(){
        defaults.clear();
    }

    public static void removeAllWorlds(){worldRooms.clear();}

    public static void removeAllMains(){
        rooms.clear();
    }

    public static void removeAllPlayerRooms(){
        playerRooms.clear();
    }

}