package us.mazecraft.roomychat;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static us.mazecraft.roomychat.Main.format;

/**
 * Created by Patman on 7/2/2016.
 */

public class playerChat implements Listener {

    @EventHandler
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        ArrayList<String> main = roomControl.getMain(player);
        if (main != null){
            if (main.get(1).isEmpty()){
                event.setFormat(format( ChatColor.GOLD + player.getDisplayName() + ChatColor.WHITE + ": " + event.getMessage()));
            }
            else{
                event.setFormat(format(main.get(1) + ChatColor.WHITE + " - " + ChatColor.GOLD + player.getDisplayName() + ChatColor.WHITE + ": " + event.getMessage()));
            }
            ArrayList<Player> players = roomControl.getRoomPlayers(main.get(0));
            event.getRecipients().retainAll(players);
            if (players.size() == 1){
                player.sendMessage("§cHey! §7You're talking to yourself!");
            }


        }
        else{
            event.setCancelled(true);
            player.sendMessage("Oops! Something went wrong. Try and set your main chatroom with \"/chat set {chatroom}\"");
            player.sendMessage("Use /chat help for more information.");
        }

    }
}
