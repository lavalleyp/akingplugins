package us.mazecraft.roomychat;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

import static us.mazecraft.roomychat.Main.new_version;

/**
 * Created by Patman on 7/2/2016.
 */

public class playerJoin implements Listener {

    private Main plugin;

    public playerJoin(Main plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        this.plugin.loadPlayersRooms(event.getPlayer());
        if(event.getPlayer().hasPermission("chat.config")){
            if(new_version){
                event.getPlayer().sendMessage("§dA new version of RoomyChat is available!");
            }
        }
    }
}
