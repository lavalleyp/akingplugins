/**
 * Created by Alteranking on 4/9/2016.
 */
package us.mazecraft.roomychat;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main extends JavaPlugin {

    public roomControl controller = new roomControl(this);
    public File configFile = new File(getDataFolder(), "config.yml");
    public File roomsFile = new File(getDataFolder(), "rooms.yml");
    public YamlConfiguration config = new YamlConfiguration();
    public YamlConfiguration worldsConfig = new YamlConfiguration();

    public String pluginVersion = this.getDescription().getVersion();
    public static String configVersion = null;
    public static Boolean update_check = false;
    public static Boolean new_version = false;
    private static String URLbase = "https://api.curseforge.com/servermods/files?projectIds=";
    private static String projectID = "259047";
    public static String URL = URLbase + projectID;
    public static Boolean global = true;
    public static Boolean global_default = true;
    public static Boolean world_rooms = true;
    public static Boolean world_switching = true;
    public static String on_join = "last_room";
    public static Integer max_limit = -1;
    public static Integer default_limit = -1;
    public static String default_prefix = "&f[&6$room&f]";
    public static String default_description = "&eA room to chat in.";
    public static String logging_level = "debug";

    @Override
    public void onEnable() {

        // Load Config
        loadConfigs();

        if(isDebugEnabled()){
            getLogger().info("Setting global as: " + global.toString());
            getLogger().info("Setting global_default as: " + global_default.toString());
            getLogger().info("Setting worlds as: " + world_rooms.toString());
            getLogger().info("Setting world_switching as: " + world_switching.toString());
            getLogger().info("Setting on_join as: " + on_join);
            getLogger().info("Setting max_limit as: " + max_limit.toString());
            getLogger().info("Setting default_limit as: " + default_limit.toString());
            getLogger().info("Setting default_prefix as: " + default_prefix);
            getLogger().info("Setting default_description as: " + default_description);
            getLogger().info("Setting update_check as: " + update_check.toString());
        }

        // Register Listeners
        getServer().getPluginManager().registerEvents(new playerJoin(this), this);
        getServer().getPluginManager().registerEvents(new playerLeave(this), this);
        getServer().getPluginManager().registerEvents(new playerChat(), this);
        getServer().getPluginManager().registerEvents(new playerChangedWorld(), this);

        // Register command "chat"
        this.getCommand("chat").setExecutor(new CommandChat(this));

        if (update_check){
            checkForUpdate();
        }

        if (new_version){
            getLogger().info("!!!!!!!!!!!");
            getLogger().info("New Version of RoomyChat Available!");
            getLogger().info("!!!!!!!!!!!");
        }
    }

    @Override
    public void onDisable(){

        saveConfigs();

        // Remove all online players from their rooms
        controller.removeAllRooms();
        controller.removeAllDefaults();
        controller.removeAllMains();
        controller.removeAllPlayerRooms();
        controller.removeAllWorlds();

    }

    public static String format(String format){
        return ChatColor.translateAlternateColorCodes('&', format);
    }

    private void saveConfigs(){

        // Get all rooms
        ArrayList<room> rooms = controller.getRooms();
        for(room room : rooms){
            // Write to configuration
            String name = room.getName();
            String prefix = room.getPrefix();
            String description = room.getDescription();
            Integer limit = room.getLimit();
            worldsConfig.set("rooms." + name + ".prefix", prefix);
            worldsConfig.set("rooms." + name + ".description", description);
            worldsConfig.set("rooms." + name + ".limit", limit);
        }
        // Get Defaults
        ArrayList<String> defaults = controller.getDefaults();
        worldsConfig.set("defaults", defaults.toArray());

        // Save configs
        config.set("global", global);
        config.set("global_default", global_default);
        config.set("world_rooms", world_rooms);
        config.set("world_switching", world_switching);
        config.set("on_join", on_join);
        config.set("max_limit", max_limit);
        config.set("default_limit", default_limit);
        config.set("default_prefix", default_prefix);
        config.set("default_description", default_description);
        config.set("update_check", update_check);
        config.set("logging_level", logging_level);

        try{
            config.save(configFile);
            worldsConfig.save(roomsFile);
        }
        catch(IOException e) {
            getLogger().warning("Couldn't save config files.");
            getLogger().warning(e.toString());
        }

        getServer().getOnlinePlayers().forEach(player -> savePlayersRooms(player));
    }

    private void loadConfigs(){
        try {
            if(!configFile.exists()){
                configFile.getParentFile().mkdirs();
                if(isDebugEnabled())
                    getLogger().info("No config found, creating new one.");
                copy(getResource("config.yml"), configFile);
            }
            if(!roomsFile.exists()){
                roomsFile.getParentFile().mkdirs();
                if(isDebugEnabled())
                    getLogger().info("No world config found, creating new one.");
                roomsFile.createNewFile();
            }

            config.load(configFile);

            worldsConfig.load(roomsFile);

            // Get configuration values
            configVersion = config.getString("version");
            global = config.getBoolean("global");
            if (global == null){
                global = true;
            }

            global_default = config.getBoolean("global_default");
            if (global_default == null){
                global_default = true;
            }

            world_rooms = config.getBoolean("world_rooms");
            if (world_rooms == null){
                world_rooms = true;
            }

            world_switching = config.getBoolean("world_switching");
            if (world_rooms == false){
                world_switching = false;
            }
            if (world_switching == null && world_rooms){
                world_switching = true;
            }
            if(world_switching == null && !world_rooms){
                world_switching = false;
            }

            on_join = config.getString("on_join");
            if (on_join == "null"){
                on_join = "last_room";
            }
            else if (on_join != "last_room" && on_join != "world" && on_join != "global"){
                on_join = "last_room";
            }
            else if (on_join == "world" && world_rooms == false){
                on_join = "last_room";
            }
            else if (on_join == "global" && global == false){
                on_join = "last_room";
            }

            max_limit = config.getInt("max_limit");
            if (max_limit == null){
                max_limit = -1;
            }

            default_limit = config.getInt("default_limit");
            if (default_limit == null){
                default_limit = -1;
            }

            default_prefix = config.getString("default_prefix");
            if (default_prefix == null){
                default_prefix = "&f[&6$room&f]";
            }

            default_description = config.getString("default_description");
            if (default_description == null){
                default_description = "&eA room to chat in.";
            }

            update_check = config.getBoolean("update_check");
            if (update_check == null){
                update_check = false;
            }

            logging_level = config.getString("logging_level");


        } catch (IOException e) {
            getLogger().warning("Error opening config Files.");
        }
        catch (InvalidConfigurationException e){
            getLogger().warning("Invalid configuration Files.");
        }

        // Add rooms from file
        try{
            // All rooms in file
            Set<String> roomsToAdd = worldsConfig.getConfigurationSection("rooms").getKeys(false);
            for (String roomToAdd : roomsToAdd) {
                if (!controller.exists(roomsToAdd.toString())){
                    String prefix = worldsConfig.getString(String.format("%s%s%s", "rooms.", roomToAdd, ".prefix"));
                    String description = worldsConfig.getString(String.format("%s%s%s", "rooms.", roomToAdd, ".description"));
                    Integer limit = worldsConfig.getInt(String.format("%s%s%s", "rooms.", roomToAdd, ".limit"));
                    if(isDebugEnabled())
                        getLogger().info(String.format("%s%s%s","Creating \"", roomToAdd,"\" as room"));
                    controller.addRoom(roomToAdd, limit,prefix, description);
                }
                else{
                    getLogger().warning(String.format("%s%s%s", "Room \"", roomToAdd, "\" already exists!"));
                }
            }

            // Add defaults from file
            List<String> defaultsToAdd = worldsConfig.getStringList("defaults");
            for (String defaultToAdd : defaultsToAdd){
                if (controller.exists(defaultToAdd)){
                    if (!controller.getDefaults().contains(defaultToAdd)){
                        if(isDebugEnabled())
                            getLogger().info(String.format("%s%s%s", "Adding room \"", defaultToAdd, "\" as default"));
                        controller.addDefaultRoom(defaultToAdd);
                    }
                }
                else{
                    getLogger().warning(String.format("%s%s%s", "Can't add room \"", defaultToAdd, "\" as default, it's not a room!"));
                }
            }
        }
        catch (NullPointerException e){
            if(isDebugEnabled())
                getLogger().info("No rooms in worlds.yml.");
        }

        if (global == true ){
            if(!controller.exists("global")){
                if(isDebugEnabled())
                    getLogger().info("Creating a global chat room.");
                controller.addRoom("global", default_limit, default_prefix, default_description);
            }
        }
        if (global_default == true){
            if (controller.exists("global")){
                if (!controller.getDefaults().contains("global")){
                    if(isDebugEnabled())
                       getLogger().info("Adding \"global\" as a default room");
                    controller.addDefaultRoom("global");
                }
            }
            else {
                getLogger().warning("Can't add room \"global\" as default, it's not a room!");
            }
        }
        if (world_rooms == true){
            List<World> worlds = new ArrayList<>(getServer().getWorlds());
            for(World world : worlds){
                if(!controller.exists(world.getName())){
                    if(isDebugEnabled())
                        getLogger().info(String.format("%s%s%s", "Creating world \"", world.getName(), "\" as room"));
                    controller.addRoom(world.getName());
                }
                controller.addWorldRoom(world.getName());
            }
        }

        getServer().getOnlinePlayers().forEach(player -> loadPlayersRooms(player));

    }

    public void savePlayersRooms(Player player){
        File playersFolder = new File(getDataFolder() + "/players/");
        if (!playersFolder.exists()){
            playersFolder.mkdir();
        }
        File playerFile = new File(playersFolder,player.getUniqueId().toString() +  ".yml");
        if (!playerFile.exists()){
            try {
                playerFile.createNewFile();
            }
            catch (IOException e){
                getLogger().warning("Can't create player file.");
            }
        }
        YamlConfiguration playerConfig = new YamlConfiguration();
        try{
            playerConfig.load(playerFile);
            playerConfig.set("rooms", roomControl.getPlayerRooms(player).toArray());
            playerConfig.set("main", roomControl.getMain(player).get(0));
            playerConfig.save(playerFile);
        }
        catch (InvalidConfigurationException e){
            getLogger().warning(String.format("%s%s%s", "Player file for player ", player.getName(), " has been corrupted!"));
        }
        catch (IOException e){
            getLogger().warning("Can't load player file.");
        }
        catch (NullPointerException e){
            try{
                playerConfig.save(playerFile);
            }
            catch(IOException error){
                getLogger().warning("Can't load player file.");
            }
        }
        roomControl.removeAll(player);
    }

    public void loadPlayersRooms(Player player){
        Boolean firstJoin = false;
        File playersFolder = new File(getDataFolder() + "/players/");
        if (!playersFolder.exists()){
            playersFolder.mkdir();
        }
        File playerFile = new File(playersFolder, player.getUniqueId().toString() +  ".yml");
        if (!playerFile.exists()){
            firstJoin = true;
            try {
                playerFile.createNewFile();
            }
            catch (IOException e){
                getLogger().warning("Can't create player file.");
            }
        }
        YamlConfiguration playerConfig = new YamlConfiguration();
        try{
            playerConfig.load(playerFile);
        }
        catch (InvalidConfigurationException e){
            getLogger().warning(String.format("%s%s%s", "Player file for player ", player.getName(), " has been corrupted!"));
        }
        catch (IOException e){
            getLogger().warning("Can't load player file.");
        }

        // Add all previous rooms to player's set
        if(!firstJoin){
            List<String> roomsToAdd = playerConfig.getStringList("rooms");
            for (String roomToAdd : roomsToAdd){
                if (!controller.getWorldRooms().contains(roomToAdd))
                controller.addPlayerToRoom(player, roomToAdd);
            }
        }


        // Add all current defaults to player's rooms
        ArrayList<String> defaults = controller.getDefaults();
        defaults.forEach(room -> controller.addPlayerToRoom(player, room));

        // Add current world if world_switching is true
        if(world_rooms && world_switching){
            controller.addPlayerToRoom(player, player.getWorld().getName());
        }

        // Set main based on variable on_join
        if (on_join.equals("global")){
            controller.addPlayerToRoom(player, "global");
            controller.setMain(player, "global");
        }
        else if (on_join.equals("world")){
            controller.addPlayerToRoom(player, player.getWorld().getName());
            controller.setMain(player, player.getWorld().getName());
        }
        else{
            if (firstJoin){
                if (world_rooms && world_switching){
                    controller.addPlayerToRoom(player, player.getWorld().getName());
                    controller.setMain(player, player.getWorld().getName());
                }
                else if (global && global_default){
                    controller.addPlayerToRoom(player, "global");
                    controller.setMain(player, "global");
                }
                else {
                    controller.setMain(player, defaults.get(0));
                }
            }
            else{
                controller.setMain(player, playerConfig.getString("main"));
            }

        }
    }

    public static boolean checkForUpdate(){
        try {
            String json = readUrl(URL);
            JSONParser jsonParser=new JSONParser();
            Object object = jsonParser.parse(json);
            JSONArray files = (JSONArray)object;

            JSONObject latestUpdate = (JSONObject) files.get(files.size() - 1);
            String name = (String) latestUpdate.get("name");

            if(!name.equalsIgnoreCase(configVersion)){
                new_version = true;
                return true;
            }
            else {
                new_version = false;
                return false;
            }
        }
        catch (Exception e){
            return false;
        }
    }

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    public static Boolean isDebugEnabled(){
        return logging_level.equalsIgnoreCase("debug");
    }

    public void reload(){
        getServer().getOnlinePlayers().forEach(player -> savePlayersRooms(player));
        saveConfigs();
        loadConfigs();
        getServer().getOnlinePlayers().forEach(player -> loadPlayersRooms(player));
    }

    private void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
            out.close();
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}