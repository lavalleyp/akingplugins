package us.mazecraft.roomychat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;

/**
 * Created by Patman on 12/4/2016.
 */
public class playerChangedWorld implements Listener {


    @EventHandler
    public void onWorldChange(PlayerChangedWorldEvent event){
        if(Main.world_switching){
            Player player = event.getPlayer();
            String newWorld = event.getPlayer().getWorld().getName();
            String oldWorld = event.getFrom().getName();
            if(!roomControl.exists(newWorld)){
                roomControl.addRoom(newWorld);
                roomControl.addWorldRoom(newWorld);
            }
            roomControl.addPlayerToRoom(player, newWorld);
            try{
                if (roomControl.getMain(player).get(0).equals(oldWorld)){
                    roomControl.setMain(player, newWorld);
                    player.sendMessage("§6You're now chatting in the §b" + newWorld + " §6chatroom");
                }
                else
                    player.sendMessage("§6You're now listening to the §b" + newWorld + " §6chatroom");
            }
            catch (NullPointerException e){
                // Do nothing
            }
        }
    }
}
